
import fasthttpd;

void main()
{
    auto server = new HttpServer;
    server.Listen(8080);

    server.Get("/", (HttpContext httpContext) {
        httpContext.response().set_body("Hello fasthttpd ;)");
        httpContext.send(httpContext.response().to_buffer());
    });

    server.Get("/world", (HttpContext httpContext) {
        httpContext.response().set_body("Hello world");
        httpContext.send(httpContext.response().to_buffer());
    });

    server.Get("/user/{id:\\d+}", (HttpContext httpContext) {
        httpContext.response().set_body("User id: " ~ httpContext.request.parameters["id"]);
        httpContext.send(httpContext.response().to_buffer());
    });

    server.Get("/blog/{name}", (HttpContext httpContext) {
        httpContext.response().set_body("Username: " ~ httpContext.request.parameters["name"]);
        httpContext.send(httpContext.response().to_buffer());
    });

    server.Post("/upload", (HttpContext httpContext) {
        httpContext.response().set_body("Using post method!");
        httpContext.send(httpContext.response().to_buffer());
    });

    server.Start();
}
