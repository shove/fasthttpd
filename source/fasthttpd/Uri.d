module fasthttpd.Uri;

class Uri
{
public:

	this(string uri)
	{
		// TODO parse
	}

    /*
     * Set the full URI string
     *
     * @param uri the full uri as a string
     */
    void set_URI(string uri)
    {
        // TODO
    }

    /*
     * Set the URI path string
     *
     * @param the uri path string
     */
    void set_path(string path)
    {
        // TODO
    }

    /*
     * Set the URI query string
     *
     * @param query the uri query string
     */
    void set_query(string query)
    {
        // TODO
    }

    /*
     * Set the URI fragment string
     *
     * @param fragment the uri fragment string
     */
    void set_fragment(string fragment)
    {
        // TODO
    }

    //  -----  URI component selectors  -----

    /*
     * For uri: "/foo/bar?test=one#element"
     *
     * "/foo/bar?test=one"
     *
     * @return the full uri
     */
    string URI()
    {
        // TODO

		return null;
    }

    /*
     * For uri: "/foo/bar?test=one#element"
     *
     * "/foo/bar"
     *
     * @return the uri path
     */
    string path()
    {
        // TODO

		return null;
    }

    /*
     * For uri: "/foo/bar?test=one#element"
     *
     * "test=one"
     *
     * @return the uri query
     */
    string query()
    {
        // TODO

		return null;
    }

    /*
     * For uri: "/foo/bar?test=one#element"
     *
     * "element"
     *
     * @return the uri fragment
     */
    string fragment()
    {
        // TODO

		return null;
    }

private:
    string _uri;
    string _path;
    string _query;
    string _fragment;
}

/*
 * URL-encode a string.
 *
 * This method will escape special characters with % symbols to conform to the
 * HTTP URL encoding scheme.
 *
 * @param s the input string to encode
 *
 * @return the encoded input string
 */
string query_escape(string s)
{
	// TODO

	return null;
}

/*
 * URL-decode a string.
 *
 * This method will unescape special characters with % symbols that conform to
 * the HTTP URL encoding scheme.
 *
 * @param s the input string to decode
 *
 * @return the decoded input string
 */
string query_unescape(string s)
{
	// TODO

	return null;
}
