module fasthttpd.HttpRequestParserHandler;

import fasthttpd.HttpRequest;
import fasthttpd.HttpMessageParser;

import std.stdio : writeln;
import std.conv : to;

import fasthttpd.Url;

class HttpRequestParserHandler : HttpMessageHandler
{
    this(HttpRequest request = new HttpRequest)
    {
        this._request = request;
    }

    void onMethod(const(char)[] method)
    {
        _request.set_method(getHttpMethodFromString(method.to!string));
    }

    void onUri(const(char)[] uri)
    {
        _request.set_uri(Url(uri.to!string));
    }

    int onVersion(const(char)[] ver)
    {
        auto minorVer = parseHttpVersion(ver);
        return minorVer >= 0 ? 0 : minorVer;
    }

    void onHeader(const(char)[] name, const(char)[] value)
    {
        _request.set_header(name.to!string, value.to!string);
    }

    void onStatus(int status) {
        // Request 不处理
    }

    void onStatusMsg(const(char)[] statusMsg) {
        // Request 不处理
    }

    HttpRequest request()
    {
        return _request;
    }

    void reset()
    {
        _request = new HttpRequest;
    }

    private HttpRequest _request;
}
