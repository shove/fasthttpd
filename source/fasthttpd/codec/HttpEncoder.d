module fasthttpd.codec.HttpEncoder;

import gear.codec.Encoder;

import gear.buffer.Buffer;

import fasthttpd.HttpRequest;
import fasthttpd.HttpResponse;

// enum string ResponseContent = "HTTP/1.1 200 OK\r\nContent-Length: 13\r\nConnection: Keep-Alive\r\nContent-Type: text/plain\r\nServer: Hunt/1.0\r\nDate: Wed, 17 Apr 2013 12:00:00 GMT\r\n\r\nHello, world!";

class HttpEncoder : AbstractEncoder
{
    override Buffer Encode(Object message)
    {
        HttpResponse response = cast(HttpResponse)message;
        assert(response !is null);

        return Buffer(cast(string) response.to_buffer());
    }
}
