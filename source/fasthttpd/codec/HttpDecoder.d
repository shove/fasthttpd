module fasthttpd.codec.HttpDecoder;

import gear.codec.Decoder;
import gear.codec.Encoder;
import gear.buffer.Buffer;
import gear.buffer.Bytes;

import gear.event;

import fasthttpd.HttpRequestParser;
import fasthttpd.HttpRequest;
import fasthttpd.HttpContext;

import gear.logging;

class HttpDecoder : AbstractDecoder
{
    private
    {
        HttpRequestParser _parser = new HttpRequestParser;
    }
    
    override void Decode(Bytes bytes)
    {
        // FIXME: Needing refactor or cleanup -@zhangxueping at 2022-04-30T11:58:30+08:00
        // 
        Buffer buf;
        buf.Append(bytes);

        string data = buf.toString();

        long result = _parser.parse(data);
        
        if ( result > 0 )
        {
            auto request = _parser.request();

            _parser.reset();

            _handler(request);
        }
    }
}
