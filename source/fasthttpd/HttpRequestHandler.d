module fasthttpd.HttpRequestHandler;

public import fasthttpd.HttpContext;

alias void delegate(HttpContext httpContext) HttpRequestHandler;
