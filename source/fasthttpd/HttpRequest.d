module fasthttpd.HttpRequest;

import fasthttpd.Url;

public import fasthttpd.Uri;
public import fasthttpd.HttpMethod;
public import fasthttpd.MultiPart;

class HttpRequest
{
private:
    HttpMethod     _method;
    Url            _uri;
    string         _path;
    string         _queryString;
    string         _HTTP_version;
    string         _source;
    string[string] _headers;
    string         _body;

public:

    /*
     * Set the HTTP method of this request.
     *
     * @param method the HTTP method
     */
    void set_method(HttpMethod method)
    {
        _method = method;
    }

    /*
     * Set the destination of this request.
     *
     * The destination is the URL path of the request, used to determine which resource is being
     * requested.
     *
     * @param destination the URI of the request
     */
    void set_uri(Url uri)
    {
        _uri = uri;
        _path = _uri.path;
    }

    void set_path(string path)
    {
        _path = path;
    }

    void set_query(string queryString)
    {
        _queryString = queryString;
    }

    /*
     * Set the HTTP version of this request.
     *
     * Sets the HTTP protocol version of this request.
     *
     * @param http_version the HTTP protocol version
     */
    void set_HTTP_version(string http_version)
    {
        // TODO
    }

    /*
     * Set the source of this request.
     *
     * Sets the address to be associated with the client of this request.
     *
     * @param source the source address
     */
    void set_source(string source)
    {
        // TODO
    }

    /*
     * Set a header value of this request.
     *
     * @param header the key of the header to be set
     * @param value the value of the header
     */
    void set_header(string header, string value)
    {
        _headers[header] = value;
    }

    /*
     * Set the body of the request.
     *
     * @param body the body of the request
     */
    void set_body(string body)
    {
        // TODO
    }

    /*
     * Obtain a reference to the URL of this request.
     *
     * @return a reference to the URL
     */
    Uri uri()
    {
        // TODO

        return null;
    }

    string path()
    {
        return _path;
    }

    /*
     * Get the HTTP method of the request.
     *
     * @return HTTP method of the request
     */
    HttpMethod method()
    {
        return _method;
    }

    /*
     * Get the HTTP version of the request.
     *
     * @return HTTP version of the request
     */
    string HTTP_version()
    {
        // TODO
        
        return null;
    }

    /*
     * Get the source of this request.
     *
     * @return the address of the source
     */
    string source()
    {
        // TODO
        
        return null;
    }

    /*
     * Get a header value from this request.
     *
     * @param name the key of the header to obtain
     *
     * @return either the header value, or an empty string if the header does not exist
     */
    string header(string name)
    {
        return _headers.get(name, "");
    }

    string[string] headers()
    {
        return _headers;
    }

    /*
     * Get the body of the request.
     *
     * @return the body of the request
     */
    string body()
    {
        // TODO
        
        return null;
    }

public:

    string[string] query;
    string[string] parameters;
    string[string] fields;
    MultiPart[] files;
}
