module fasthttpd.MultiPart;

struct MultiPart
{
	string[string] headers;
	string name;
	string value;
	string filename;
	long filesize = 0;
	string filepath;
}
